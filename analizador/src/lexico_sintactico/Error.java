/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexico_sintactico;

/**
 *
 * @author Guillermo Marquez
 */
public class Error {
    int num;
    String location;
    String desc;

    public Error(int num, String location, String desc){
        this.num=num;
        this.location=location;
        this.desc=desc;
    }

    public int getNum(){
        return num;
    }

    public String getLocation(){
        return location;
    }

    public String getDesc(){
        return desc;
    }
}
