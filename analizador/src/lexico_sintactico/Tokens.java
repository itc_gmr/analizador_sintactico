/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexico_sintactico;

/**
 *
 * @author Guillermo Marquez
 */
public class Tokens {
    //CLASE DE TOKENS PARA CREAR OBJETOS QUE TENGAN DOS ATRIBUTOS, TOKEN Y TIPO
	
    String token;
    String tipo;
    int code;

    public Tokens(String t, String tp){
            token = t;
            tipo = tp;

    }
    public Tokens(String t, String tp, int n){
            token = t;
            tipo = tp;
            code=n;

    }

    public void setCode(int n){
            code=n;
    }
    public int getCode(){
            return code;
    }
    public String getToken(){
            return token;
    }
    public String getTipo(){
            return tipo;
    }
    public boolean esInicio(){
            boolean res=false;
            if (token=="INICIO")
                    res=true;
            return res;
    }
    public boolean esFin(){
            boolean res=false;
            if (token=="FIN")
                    res=true;
            return res;
    }

}
