/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexico_sintactico;

/**
 *
 * @author Guillermo Marquez
 */
public class Begin extends Statement{
    Tokens i;
    Expresion expr;
    Statement es;
    String Clasificacion;
    
    Begin(Tokens IF, Expresion e, Statement state, String Clasif){
        super(state.id,state.operador,state.expr,state.Clasificacion);
        this.i=IF;
        this.expr=e;
        this.Clasificacion=Clasif;
    }

    Begin(Tokens I, String Clasif){
        
        i=I;
        Clasificacion=Clasif;
    }
    
    public String getClasificacion(){
        return Clasificacion;
    }


    public String getIF() {
        return i.getToken();
    }

    public Expresion getExpresion() {
        return expr;
    }

    public String toString(){
        return i.getToken()+"("+expr.toString()+")";
    }
}
