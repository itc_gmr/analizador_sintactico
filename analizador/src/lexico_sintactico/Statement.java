/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexico_sintactico;

/**
 *
 * @author Guillermo Marquez
 */
public class Statement {
    	Tokens id=Parser.variable;
	Tokens operador;
	Expresion expr;
	String 	Clasificacion;
	
	
	Statement(){
		id=Parser.variable;
		operador=Parser.variable;
		expr=new Expresion();
	}
	
	//NUEVA MODIFICACION
	Statement(Tokens iden, Tokens op, Expresion e, String Clasif){
		id=iden;
		operador=op;
		expr=e;
		Clasificacion=Clasif;
	}
        
            Statement(Tokens iden, String Clasif){
		id=iden;
		operador=new Tokens("","");
		expr=new Expresion(new Tokens("",""),"");
		Clasificacion=Clasif;
	}
	
	//NUEVO
	public String getClasificacion(){
		return Clasificacion;
	}
	
	public Tokens getId() {
		return id;
	}

	public Tokens getOperador() {
		return operador;
	}

	public Expresion getExpr() {
		return expr;
	}
	
	public String toString()
	{
		return (id.getToken()+operador.getToken()+expr.toString());
	}
	
	public boolean isStatement()
	{
		if(id.getToken().equals(""))
			return false;
		return true;
	}
}
