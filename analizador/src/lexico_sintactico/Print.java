/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexico_sintactico;

/**
 *
 * @author Guillermo Marquez
 */
public class Print extends Statement{
    Tokens i;
    Expresion expr;
    Statement es;
    String Clasificacion;
    
    Print(Tokens I, Expresion e, String Clasif){
        i=I;
        expr=e;
        Clasificacion=Clasif;
    }
    
    public String getClasificacion(){
        return Clasificacion;
    }


    public String getPrint() {
        return i.getToken();
    }

    public Expresion getExpresion() {
        return expr;
    }

    public String toString(){
        return i.getToken()+"("+expr.toString()+")";
    }
}
