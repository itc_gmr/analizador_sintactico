/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexico_sintactico;

/**
 *
 * @author Guillermo Marquez
 */
public class VarDeclaration {
    Tokens type,id;

    VarDeclaration(Tokens tipo, Tokens ident){
        type=tipo;
        id=ident;
    }

    public String getType() {
        return type.getToken();
    }

    public String getId() {
        return id.getToken();
    }
}
