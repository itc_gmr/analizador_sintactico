/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lexico_sintactico;

import java.util.Vector;
import javax.swing.plaf.synth.SynthSeparatorUI;

public class Parser {
    	
	int index;
	Vector <Tokens> tokens;
	Vector <Error> errors;
	
	Vector <VarDeclaration> VarDec; //VECTOR PARA GUARDAR DECLARACIONES DE VARIABLES
	Vector <Statement> States; // VECTOR PARA UARDAR STATEMENTS
	Vector <Cuadruple> Cuadruples;
	Vector <Cuadruple> Wails;
	//VECTORES PARA ALMACENAR LA INFORMACION DE LAS VARIABLES DECLARADAS
	
	Vector <String> ID;
	Vector <String> Tipo;
	
	static Tokens variable; 
	static String Estring;
	
	static int Cont=0;
	static int Jumps=0;
	static int saltos=0;
	
	static int inutil;
	static int basura;
	
	
	int contError;
	int lines;
	Tokens currentToken;
  	String res="";
  	String ressem="";
  	
  	boolean Correcto=true;
        
    //Constructor del parser. Inicializ variables  y llama al m�todo que hace el trabajo
    public Parser(Vector<Tokens> tokens){
        this.tokens=tokens;
        index=0;//inicia en el token de la posici�n 0
        nextToken();
        variable = new Tokens("","INVALIDO",-1);
        errors= new Vector <Error>();
        VarDec= new Vector <VarDeclaration>();
        States= new Vector <Statement>();
        Cuadruples= new Vector <Cuadruple>();
        Wails= new Vector <Cuadruple>();

        Tipo = new Vector <String>();
        ID = new Vector <String>();

        lines=0;
        Program();
    }
    //GRAMATICA
/*
Program -> VarDeclaration* Statement* <EOF>
VarDeclaration -> ("int | float") id ";" 
Statement -> "if" Expresion "then" Statement "else" Statement
Statement -> "begin" Statement Last
Statement -> "print" Expesion
Last -> "end"
Last -> ; Statement Last
Expresion -> "num" = "num"
Expresion -> "id" = "id"
*/
    public void Program(){
    //IDENTIFICADOR, IF
        if(!(currentToken.getCode()== 6 || currentToken.getCode()== 7)){
            System.out.println("No declaro variables");
            errors.addElement(new Error(errors.size()+1,"Codigo Invalido","Variable mal declarada"));
        }
        System.out.println("Declaro variables");
        VarDeclaration();    
        while(currentToken.getCode() == 0 || currentToken.getCode() == 3 || currentToken.getCode() == 5){
            if(Statements()){
                nextToken();
            }
            
        }
        
        System.out.println("");
        System.out.println("ARBOL DE DECLARACION DE VARIABLES");
        System.out.println("");

        for(int i=0;i<VarDec.size();i++){
            System.out.println(VarDec.elementAt(i).getType()+"    "+VarDec.elementAt(i).getId());
        }
        
        System.out.println("");
        System.out.println("ARBOL DE ESTATUTOS");
        System.out.println("");
        
        if(!(errors.size()==0)){		
            System.out.println("El progrma tiene errores, por lo tanto no se genera arbol sintactico de Statements");
        }          
        System.out.println("");
        for(int i=0; i<States.size(); i++){
            System.out.println(States.elementAt(i).toString());
        }
        
        printRes();
        System.out.println("Respuesta: \n " + res);
        String Cuadruple="";
        if(!Semantica()){
            ressem=ressem+"\nEl programa tiene errores de semantica";
            return;
        }
        ressem=ressem+"El programa es correcto\n No hay errores de Semantica";
        CodigoIntermedio();
        Cuadruples.addElement(new Cuadruple((Cuadruples.size()+1),"","","",""));
        int posi;	
        //CHECAR LOS JUMPS 
        System.out.println("");
        System.out.println("\tCODIGO INTERMEDIO");
        System.out.println("\t   CUADRUPLES");
        System.out.println("-----------------------------------------");
        for(int i=Cuadruples.size()-1;i>=0;i--){
            if(Cuadruples.elementAt(i).COJ.equals("Jump")){
                if(i>0 && Cuadruples.elementAt(i-1).COJ.equals("Jump")){
                    for(int j=0; j<Cuadruples.size();j++){
                        if(Cuadruples.elementAt(j).COJ.equals("JZ") && Cuadruples.elementAt(j).RS.equals("&")){
                            if(j>0 && Cuadruples.elementAt(j-1).COJ.equals("CMP")){
                                Cuadruples.elementAt(i).setRS(""+Cuadruples.elementAt(j-1).Posicion);
                                Cuadruples.elementAt(j).setRS(""+Cuadruples.elementAt(i+1).Posicion);	 		
                                break;
                            }
                            else{
                                Cuadruples.elementAt(i).setRS(""+Cuadruples.elementAt(j).Posicion);
                                Cuadruples.elementAt(j).setRS(""+Cuadruples.elementAt(i+1).Posicion);
                                break;
                            }
                        }
                    } 						
                }
                else{
                    for(int j=i; j>0;j--){
                        if(Cuadruples.elementAt(j).COJ.equals("JZ") && Cuadruples.elementAt(j).RS.equals("&")){
                            if(Cuadruples.elementAt(j-1).COJ.equals("CMP")){
                                Cuadruples.elementAt(i).setRS(""+Cuadruples.elementAt(j-1).Posicion);
                                Cuadruples.elementAt(j).setRS(""+Cuadruples.elementAt(i+1).Posicion);
                                break;
                            }
                            else{
                                Cuadruples.elementAt(i).setRS(""+Cuadruples.elementAt(j).Posicion);
                                Cuadruples.elementAt(j).setRS(""+Cuadruples.elementAt(i+1).Posicion);
                                break;
                            }
                        }
                    } 					 					
                }
            }
        }


        for(int i=0; i<Cuadruples.size(); i++){
            if(Cuadruples.elementAt(i).COJ.equals("JZ") && Cuadruples.elementAt(i).RS.equals("#")){
                for(int j=i;j<Cuadruples.size();j++){
                    if(Cuadruples.elementAt(j).COJ.equals("=")){
                        for(int k=j;k<Cuadruples.size();k++){
                            if(Cuadruples.elementAt(k+1).COJ.equals("Jump")){
                                Jumps++;
                            }
                            else{
                                break;
                            }
                        }
                        Cuadruples.elementAt(i).setRS(""+Cuadruples.elementAt(j+1+Jumps).Posicion);
                        break;
                    }
                }
            }
            Jumps=0;
            System.out.println(Cuadruples.elementAt(i).toString());
        } 				
    }
    //Analizador Semantico
    public boolean Semantica(){
        boolean Semantica=true;
        String Var="";
        String Var2="";
        String Taip;
        String Aid;
        
        //Pregunto para saber si el analisis sintactico fue correcto
        if(res=="El programa es correcto "){
            //COMIENZO CON EL ANALISIS SEMANTICO
            //VER SI HAY VARIABLES CON EL MISMO NOMBRE REPETIDAS
            System.out.println("");
            for(int i=0; i<VarDec.size();i++){
                Var=VarDec.elementAt(i).getId();
                for(int j=i+1; j<VarDec.size();j++){
                    Var2=VarDec.elementAt(j).getId();//CUANDO ASIGO AQUI LA POSICION J POR ALGUNA RAZON SE IMPRIME DOS VECES
                    if(Var.equals(Var2)){
                        ressem="Error de Semantica -\n La variable "+Var+" esta declarada mas de una vez";
                        return Semantica=false;
                    }
                }
            }

            //COMIENZO CON EL ANALISIS SEMANTICO
            if(VarDec.isEmpty() && !States.isEmpty()){
                ressem="Error - El programa no tiene variables declaradas";
                return false;
            }

            //COMENZAR A CHECAR LOS STATEMENTS

            //Variables Auxiliares 
            Statement Actual;
            String Clasif;
            String ClasifExp;

            Expresion Xpre;
            If perro;
            Begin beg;
            Print pr;

            String id;
            String id2;
            String id3;

            //FOR PARA RECORRER TODO EL VECTOR DE STATEMENTS 
            //CHECAR QUE LAS VARIABLES ESTEN DECLARADAS 
            for(int i=0;i<States.size();i++){
                // Clasificacion A = Asignacion
                // Clasificacion B = if
                // Clasificacion C = begin
                // Clasificacion D = print

                Clasif = States.elementAt(i).getClasificacion();

                //Asignacion
                if(Clasif.equals("A"))
                {
                    //Le asigno el identificador que va antes de la asignacion 
                    id=States.elementAt(i).getId().getToken();
                    int j=0;
                    //CHECO SI EL ID ESTA DECLARADO
                    while(j<VarDec.size())
                    {
                        System.out.println(VarDec.elementAt(j).getId());
                        String Aux=VarDec.elementAt(j).getId();

                        if(id.equals(Aux))
                        {
                            j=VarDec.size();
                        }
                        else if(j==VarDec.size()-1 && !id.equals(Aux))
                        {
                            ressem="Error de Semantica -\n La variable "+id+" NO esta declarada";
                            return Semantica = false;
                        }
                        else{
                            j++;
                        }                      
                    }	
                    Xpre=States.elementAt(i).getExpr();
                    ClasifExp=Xpre.getClasificacion();

                    //DEPENDIENDO DEL TIPOD DE EXPRESION CHECO SI EXISTEN LOS ID EN EL VECTOR DE DECLARACIONES
                    if(ClasifExp.equals("a"))
                    {
                        id2=Xpre.getId();
                        id3=Xpre.getId2();
                        
                        int k=0;
                        while(k<VarDec.size()){
                            String Aux=VarDec.elementAt(k).getId();
                            if(id2.equals(Aux)){
                                k=VarDec.size();
                            }
                            else if(k==VarDec.size()-1 && !id2.equals(Aux)){
                                ressem="Error de Semantica -\n La variable "+id2+" NO esta declarada";
                                return Semantica = false;
                            }
                            else{
                                k++;
                            }
                        }

                        int l=0;
                        while(l<VarDec.size()){
                            String Aux=VarDec.elementAt(l).getId();
                            if(id3.equals(Aux)){
                                l=VarDec.size();
                            }
                            else if(l==VarDec.size()-1 && !id3.equals(Aux)){
                                ressem="Error de Semantica -\n La variable "+id3+" NO esta declarada";
                                return Semantica = false;
                            }
                            else{
                                l++;
                            }                        
                        } 				
                    }
                    else if(ClasifExp.equals("b")){
                        id2=Xpre.getId();

                        int k=0;
                        while(k<VarDec.size())
                        {
                            String Aux=VarDec.elementAt(k).getId();
                            if(id2.equals(Aux)){
                                k=VarDec.size();
                            }
                            else if(k==VarDec.size()-1 && !id2.equals(Aux)){
                                ressem="Error de Semantica -\n La variable "+id2+" NO esta declarada";
                                return Semantica = false;
                            }
                            else{
                                k++;
                            }             
                        }
                    }
                }
                //if
                else if(Clasif.equals("B")){
                    perro=(If)States.elementAt(i);
                    Xpre=perro.getExpresion();
                    ClasifExp=Xpre.getClasificacion();
                    if(ClasifExp.equals("a")){
                        id2=Xpre.getId();
                        id3=Xpre.getId2();

                        int k=0;
                        while(k<VarDec.size()){
                            String Aux=VarDec.elementAt(k).getId();
                            if(id2.equals(Aux)){
                                k=VarDec.size();
                            }
                            else if(k==VarDec.size()-1 && !id2.equals(Aux)){
                                ressem="Error de Semantica -\n La variable "+id2+" NO esta declarada";
                                return Semantica = false;
                            }
                            else{
                                k++;
                            }
                        }
                        int l=0;
                        while(l<VarDec.size()){
                            String Aux=VarDec.elementAt(l).getId();
                            if(id3.equals(Aux)){
                                l=VarDec.size();
                            }
                            else if(l==VarDec.size()-1 && !id3.equals(Aux)){
                                ressem="Error de Semantica -\n La variable "+id3+" NO esta declarada";
                                return Semantica = false;
                            }
                            else{
                                l++;
                            }
                        } 				
                    }
                }
                //print
                else if(Clasif.equals("D")){
                    pr=(Print)States.elementAt(i);
                    Xpre=pr.getExpresion();
                    ClasifExp=Xpre.getClasificacion();
                    if(ClasifExp.equals("a")){
                        id2=Xpre.getId();
                        id3=Xpre.getId2();
                        int k=0;
                        while(k<VarDec.size()){
                            String Aux=VarDec.elementAt(k).getId();
                            if(id2.equals(Aux)){
                                k=VarDec.size();
                            }
                            else if(k==VarDec.size()-1 && !id2.equals(Aux)){
                                ressem="Error de Semantica -\n La variable "+id2+" NO esta declarada";
                                return Semantica = false;
                            }
                            else{
                                k++;
                            }
                        }
                        int l=0;
                        while(l<VarDec.size())
                        {
                            String Aux=VarDec.elementAt(l).getId();
                            if(id3.equals(Aux)){
                                l=VarDec.size();
                            }
                            else if(l==VarDec.size()-1 && !id3.equals(Aux)){
                                ressem="Error de Semantica -\n La variable "+id3+" NO esta declarada";
                                return Semantica = false;
                            }
                            else{
                                l++;
                            }
                        } 				
                    }
                }
            } 

            //CHECAR LA COMPATIBILIDAD DE TIPOS EN LAS EXPRESIONES
            //Variables auxiliares
            String Typo ="";
            String Typo2="";
            String Typo3="";

            String Op="";            
            for(int i=0;i<States.size();i++){
                Clasif = States.elementAt(i).getClasificacion();
                //ASIGNACION - Agregar la declaracion de variables
                if(Clasif.equals("A")){
                    //Le asigno el identificador que va antes de la asignacion 
                    id=States.elementAt(i).getId().getToken(); 

                    //OCUPO OBTENER EL TIPO DE DATO DEL IDENTIFICADOR
                    for(int j=0; j<VarDec.size();j++){
                        Var=VarDec.elementAt(j).getId();
                        if(Var.equals(id)){
                            Typo=VarDec.elementAt(j).getType();
                        }
                    }
                    Xpre=States.elementAt(i).getExpr();
                    ClasifExp=Xpre.getClasificacion();

                    if(ClasifExp.equals("a")){
                        id2=Xpre.getId();
                        //CHECAR EL TIPO DEL PRIMER IDENTIFICADOR DE LA EXPRESION
                        for(int j=0; j<VarDec.size();j++){
                            Var=VarDec.elementAt(j).getId();
                            if(Var.equals(id2)){
                                Typo2=VarDec.elementAt(j).getType();
                            }
                        }

                        //CHECAR EL TIPO DEL SEGUNDO IDENTIFICADOR DE LA EXPRESION
                        id3=Xpre.getId2();
                        for(int j=0; j<VarDec.size();j++){
                            Var=VarDec.elementAt(j).getId();
                            if(Var.equals(id3)){
                                Typo3=VarDec.elementAt(j).getType();
                            }
                        }
                        Op=Xpre.getOperador();
                        if(Typo2.equals("int")){
                            if((Typo3.equals("float"))){ 
                                ressem=("Error de Semantica \nNo se puede realizar una suma entera entre dos variables\n de tipo float");	
                                return Semantica=false;
                            }
                        }	
                    }
                }
                //---------------------------------------------------if
                else if(Clasif.equals("B")){
                    perro=(If)States.elementAt(i);
                    Xpre=perro.getExpresion();
                    ClasifExp=Xpre.getClasificacion();

                    if(ClasifExp.equals("a")){
                        id2=Xpre.getId();
                        id3=Xpre.getId2();
                        Op=Xpre.getOperador();
                        //CHECAR EL TIPO DE DATO DEL PRIMER IDENTIFICADOR DE LA EXPRESION
                        for(int j=0; j<VarDec.size();j++){
                            Var=VarDec.elementAt(j).getId();
                            if(Var.equals(id2)){
                                Typo2=VarDec.elementAt(j).getType();
                            }
                        }	
                        //CHECAR EL TIPO DE DATO DEL SEGUNDO IDENTIFICADOR DE LA EXPRESION
                        for(int j=0; j<VarDec.size();j++){
                            Var=VarDec.elementAt(j).getId();
                            if(Var.equals(id3)){
                                Typo3=VarDec.elementAt(j).getType();
                            }
                        }	
                        if(Typo2.equals("int")){
                            if((Typo3.equals("float"))){
                                ressem=("Error de Semantica \nNo se puede realizar una comparacion entre una variable\n de tipo entera y otra float");	
                                return Semantica=false;
                            }
                        }
                    }
                }
                //Print
                else{
                    pr=(Print)States.elementAt(i);
                    Xpre=pr.getExpresion();
                    ClasifExp=Xpre.getClasificacion(); 
                    if(ClasifExp.equals("a")){
                        id2=Xpre.getId();
                        id3=Xpre.getId2();	
                        Op=Xpre.getOperador();

                        //CHECAR EL TIPO DE DATO DEL PRIMER IDENTIFICADOR DE LA EXPRESION
                        for(int j=0; j<VarDec.size();j++){
                            Var=VarDec.elementAt(j).getId();
                            if(Var.equals(id2)){
                                Typo2=VarDec.elementAt(j).getType();
                            }
                        }	

                        //CHECAR EL TIPO DE DATO DEL SEGUNDO IDENTIFICADOR DE LA EXPRESION
                        for(int j=0; j<VarDec.size();j++){
                            Var=VarDec.elementAt(j).getId();
                            if(Var.equals(id3)){
                                Typo3=VarDec.elementAt(j).getType();
                            }
                        }	
                        if(Typo2.equals("int")){
                            if((Typo3.equals("float"))){
                                ressem=("Error de Semantica \nNo se puede realizar una asignacion entre una variable\n de tipo entera y otra float");	
                                return Semantica=false;
                            }
                        }
                    }
                }
            }
        }
        else	
        {
            ressem="El programa tiene errores de Sintaxis "
                +"\n"+"por lo que no se realiza el Analisis Semantico";
            Semantica=false;
        }		
        return Semantica;
    }
    
    //Codigo intermedio
    public void CodigoIntermedio(){
        String Tipo="";
        String Clasif;
        String ClasifExp;
        String AuxClas;
        Expresion Xpre;
        If perro;
        Print gato;

        int m=1;
        int l=1;
        int Utilidad=0;	
        for(int i=0;i<States.size();i++){
            Clasif = States.elementAt(i).getClasificacion(); //OBTENGO QUE TIPO DE STATEMENT ES
            String salto = "";
            switch (Clasif){
                case "A":
                    Xpre=States.elementAt(i).getExpr();
                    ClasifExp=Xpre.getClasificacion(); //QUE TIPO DE EXPRESION OBTENGO

                    //ESTE CASO GENERA 2 LINEAS DE CUADRUPLES
                    if(ClasifExp.equals("a")){
                        if(Xpre.getOperador().equals("=")){
                            Cuadruples.add(new Cuadruple(l,"CMP",Xpre.getId(),Xpre.getId2(),"t"+m));
                        }else{
                            Cuadruples.add(new Cuadruple(l,"+",Xpre.getId(),Xpre.getId2(),"t"+m));
                        }	
                        Cuadruples.add(new Cuadruple(++l,"=","t"+(m++),"",States.elementAt(i).getId().token));
                        if(i>0){
                            Utilidad=i-1;
                            AuxClas=States.elementAt(Utilidad).getClasificacion();	
                            System.out.println("                        "+AuxClas);
                            while(AuxClas.equals("C")&&Utilidad>=0){
                                if(Utilidad>=0){
                                    //System.out.println("AQUI CREE UN JUMP LOLX2");
                                    Cuadruples.add(new Cuadruple(++l,"Jump","","","$"));
                                    --Utilidad;
                                    if(Utilidad!=-1){
                                        AuxClas=States.elementAt(Utilidad).getClasificacion();
                                    }
                                }
                                else{
                                    Utilidad--;
                                }
                            }
                        }			
                        m++;
                    }
                    //ESTE CASO GENERA 1 LINEA DE CUADRUPLES
                    else{
                        Cuadruples.add(new Cuadruple(l,"=",Xpre.getId(),"",States.elementAt(i).getId().token));
                        if(i>0){
                            Utilidad=i-1;
                            AuxClas=States.elementAt(Utilidad).getClasificacion();	
                            System.out.println("                        "+AuxClas);
                            while(AuxClas.equals("C")&&Utilidad>=0){
                                if(Utilidad>=0){
                                    Cuadruples.add(new Cuadruple(++l,"Jump","","","$"));

                                    --Utilidad;
                                    if(Utilidad!=-1){
                                        AuxClas=States.elementAt(Utilidad).getClasificacion();
                                    }               
                                }
                                else{
                                    Utilidad--;
                                }
                            }
                        }					
                    }
                    l++;
                    break;	
                case "B":
                    perro=(If)States.elementAt(i);
                    Xpre=perro.getExpresion();
                    ClasifExp=Xpre.getClasificacion(); 
                    //ESTE CASO DE IF GENERA DOS CUADRUPLES
                    if(ClasifExp.equals("a")){
                        Cuadruples.add(new Cuadruple(l,"CMP",Xpre.getId(),Xpre.getId2(),"t"+m));
                        Cuadruples.add(new Cuadruple(++l,"JZ","t"+m,"","#"));
                        m++;
                    }
                    else{					
                        Cuadruples.add(new Cuadruple(l,"JZ",Xpre.getId(),"","#"));
                    }
                    l++;				
                    break; 
                //PRINT	
                case "D":
                    gato=(Print)States.elementAt(i);
                    Xpre=gato.getExpresion();
                    ClasifExp=Xpre.getClasificacion(); 

                    //Print(identificador==identificador)
                    if(ClasifExp.equals("a")){
                        Cuadruples.add(new Cuadruple(l,"PRT",Xpre.getId(),Xpre.getId2(),"t"+m));
                        m++;
                    }
                    else{
                        Cuadruples.add(new Cuadruple(l,"PRT",Xpre.getId(),"","&"));
                    }
                    l++;
                    break;		
            }	
        } 		
    }
    
    // Analizador Sintactico
    public boolean VarDeclaration()
    {
        boolean correcto = false;
        while(tipo_dato()){
            VarDec.add(new VarDeclaration(tipoDato(),ID()));
            if(VarDec.lastElement().getType().equals("")||VarDec.lastElement().getId().equals("")){
                 VarDec.remove(VarDec.lastElement());
            }
            //pregunta por ;
            if(currentToken.getCode() == 10){
                Correcto=true;
                System.out.println("Correcto");
                nextToken();
                if(!(tipo_dato())){
                    if(currentToken.getCode() == 0 || currentToken.getCode() == 3 ||currentToken.getCode() == 5){
                       continue; 
                    }
                    Correcto=false;
                    if(errors.size()==0){
                        errors.addElement(new Error(errors.size()+1,"Codigo incorrecto","Tipo de dato incorrecto"));
                    }
                    return false;
                }
            }
            else{
                System.out.println("Incorrecto");
                Correcto=false;
                if(errors.size()==0){
                    errors.addElement(new Error(errors.size()+1,"Signo especial","Falta punto y coma"));
                }
                return false;
            }	
        }
        return correcto;
    }
    
    public boolean Statements()
    {
        boolean correcto = false;
        int caracter;
        String Clasificacion;
        caracter = currentToken.getCode();
        Statement State = new Statement();
        switch(caracter)
        {
            //caso if
            case 0:
            {
                Tokens condicion = currentToken;
                nextToken();
                Expresion expr = Expresion(); //checar expresion						
                if(expr.isExpresion())
                {
                    Clasificacion="B";
                    States.add(new If(condicion,expr,Clasificacion));
                    if(!(currentToken.getCode() == 1)){                 
                        States.remove(States.lastElement());
                        errors.addElement(new Error(errors.size()+1,"Statement 2","Falta THEN"));
                        return correcto;
                    }
                    nextToken();
                    if(Statements()){
                        //nextToken();
                        if(!(currentToken.getCode() == 2)){
                            States.remove(States.lastElement());
                            System.out.println(currentToken.getCode() + " Esto es un error de ELSE" + currentToken.getToken());
                            errors.addElement(new Error(errors.size()+1,"Statement 2","Falta ELSE"));
                            return correcto;
                        }
                        nextToken();
                        if(!Statements()){
                            States.remove(States.lastElement());
                            errors.addElement(new Error(errors.size()+1,"Statement 2","IF mal declarado, sigue un estatuto"));
                            return correcto;
                        }
                        correcto=true;
                        return correcto;
                    }
                    else{
                        States.remove(States.lastElement());
                        errors.addElement(new Error(errors.size()+1,"Statement 2","IF mal declarado, sigue un estatuto"));
                        return correcto;
                    }
                }
                else{
                    errors.addElement(new Error(errors.size()+1,"Statement 2","IF mal declarado, sigue una expresion"));
                    return false;
                }
            }
            
            //Caso Begin 
            case 3:
            {
                Tokens beg = currentToken;
                nextToken();
                if(!(Statements())){
                    //errors.addElement(new Error(errors.size()+1,"Statement 2","Begin mal declarado"));
                    return false;
                }
                if(!(Last())){
                    errors.addElement(new Error(errors.size()+1,"Statement 2","Begin mal declarado, sigue un cierre"));
                    return false;                   
                }
                Clasificacion="C";
                //States.add(new Begin(beg,Clasificacion));
                correcto = true;
                return correcto;
            }

            //Caso Print
            case 5:
            {
                Tokens print = currentToken;
                nextToken();
                Expresion expr = Expresion();
                Clasificacion="D";
                States.add(new Print(print,expr,Clasificacion));
                correcto = true;
                if(!(expr.isExpresion())){
                    States.remove(States.lastElement());
                    errors.addElement(new Error(errors.size()+1,"Statement 2","Print mal declarado, sigue expresion"));
                    return false;   
                }
                return correcto;
            }
            default:
                return correcto;
        } 				
    }
    
    public boolean Last(){
        boolean correcto = false;
        int caracter;
        String Clasificacion;
        caracter = currentToken.getCode();
        switch(caracter){
            case 4:
            {
                correcto = true;
                return correcto;
            }
            
            case 10:
            {
                nextToken();
                if(!(Statements())){
                    errors.addElement(new Error(errors.size()+1,"Statement 2","Last mal declarado Caso 1"));
                    return false; 
                }
                if(!(Last())){
                    errors.addElement(new Error(errors.size()+1,"Statement 2","Last mal declarado Caso 2"));
                    return false; 
                }
                correcto = true;
            }
            
            default: 
                return correcto;
        }
    }
    
    public Expresion Expresion() {
        //El caso base de expresion es un token vacio 
        Expresion Expr=new Expresion(new Tokens("",""),"");

        String clasificacion;

        //pregunta por identificador
        if(currentToken.getCode()==21){
            //System.out.println("Entre a la expresion: " + currentToken.getToken());
            Tokens id = currentToken;
            nextToken();
            if(currentToken.getCode()==9){
                Tokens op = currentToken;
                nextToken();
                //pregunta por identificador 
                if(currentToken.getCode()==21)
                {
                    //-----------------------------ES UNA EXPRESION DE TIPO:
                    //-----------------------------identifier =  Identifier (==|+) Identifier
                    clasificacion="a";
                    Expr=new Expresion(id,op,currentToken,clasificacion); //aqui creo la expresion de tipo id(+|==)id
                    nextToken();
                }
                else{
                    errors.addElement(new Error(errors.size()+1,"Expresion "+lines," Se espera un id"));
                }
            }
        }
        else{
            errors.addElement(new Error(errors.size()+1,"Expresion "+lines," Se espera un id"));
        }
        return Expr;
    }
    
    public Tokens ID(){
            Tokens id=variable;
            if(currentToken.getCode()==21){
                    id=currentToken;
                    nextToken();
            }
            else
                    errors.addElement(new Error(errors.size()+1,"Declaracion de Variables "+lines, "Se espera un id"));				
            return id;
    }
    
    public boolean tipo_dato(){
        if(currentToken.getCode()== 6||currentToken.getCode()== 7){
            //System.out.println("Tipo de dato correcto");
            return true;
        }
        //System.out.println("Tipo de dato incorrecto");
        return false;
    }
    
    public Tokens tipoDato(){
        Tokens td=variable; //variable = new Tokens("INVALIDO","INVALIDO",-1);
        if (tipo_dato()){
            td=currentToken;
            nextToken();
        }
        else{
            errors.addElement(new Error(errors.size()+1,"Expresion "+lines, "Se espera un tipo de dato"));
        }              
        return td;
    }
    
    //Actualiza l variable "currentToken" al siguiente token en el vector
    public void nextToken(){
        if(index<tokens.size()){//si el �ndice es menor que el tama�o del vector
            currentToken=tokens.elementAt(index);
            index++;
        }
        else{
            currentToken= new Tokens("INVALIDO","INVALIDO",-1);
        }
    }
    
    public void printRes(){
    	if(errors.size()==0){
            res="El programa es correcto ";
    	}else{
            Correcto = false;
            res="Se encontraron "+errors.size()+" errores en el programa"+"\n";
            for(int i=0;i<errors.size();i++){
                res+="Error "+errors.elementAt(i).getNum()+": "+errors.elementAt(i).getLocation()+","+errors.elementAt(i).getDesc()+"\n";
            }
    	}
    }
    
    public boolean isCorrect(){
    	return Correcto;
    }
    
    public String Resultado(){
    	return res;
    }
    
    public String ResultadoSemantico()
    {
    	return ressem;
    }
}
